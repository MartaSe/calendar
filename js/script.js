document.addEventListener('DOMContentLoaded', function () {


        var container = document.querySelector('.calendar'),
            boxData = document.querySelector('.box_data'),
            data = new Date(),
            month = data.getMonth(),
            year = data.getFullYear(),
            actData = new Date(year,month,0),
            firstDay = actData.getDay(),
            prev = document.querySelector('.btn_prev'),
            next = document.querySelector('.btn_next'),
            monthName = [ 'Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj',
                'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik',
                'Listopad', 'Grudzień' ],
            dayName = ['Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek',
                'Sobota', 'Niedziela'];

    function changeBackground() {
       var pictures = document.querySelector('body');

       if (month >= 8 && month <= 10){
        pictures.classList.remove('summer', 'winter');
        pictures.classList.add('autumn');
       } else if (month >= 11 || month <= 1 ) {
        pictures.classList.remove('autumn', 'spring');
        pictures.classList.add('winter');
       } else if (month >= 2 && month <= 4 ) {
        pictures.classList.remove('winter', 'summer');
        pictures.classList.add('spring');
       } else if (month >= 5 && month <= 7 ) {
        pictures.classList.remove('spring', 'autumn');
        pictures.classList.add('summer');
       }
    }

    function createMonth(aa, bb) {
        container.innerHTML="";

        function daysInMonth(month, year) {
            return new Date(year, month, 0).getDate();
        }

        function getMargin() {

            var firstBox = document.getElementsByClassName('single_day_style')[0],
                divStyle = window.getComputedStyle(firstBox),
                widthDiv = parseFloat(divStyle.width),
                marginDiv = parseFloat(divStyle.marginRight),
                sum = widthDiv + marginDiv*2,
                actData = new Date(year,month,0),
                firstDay = actData.getDay();

            firstBox.style.marginLeft = sum*(firstDay) + marginDiv + 'px';
            console.log(firstBox);
        }

        for(var  i = 1; i < 13; i++) {
            var days = (daysInMonth(aa, bb));
            for(var a = 1; a <days+1; a++) {
                var singleDay = document.createElement('div');

                container.appendChild(singleDay);
                singleDay.classList.add('single_day_style');
                singleDay.innerHTML = a + '</br>' + dayName[firstDay];

                changeBackground();
                getMargin();

                firstDay++;
                if (firstDay == 7) {
                    firstDay = 0;
                }
                boxData.innerHTML = monthName[month] + ', ' + year;
            }
            return
        }
    }

    function initEvent() {

        prev.addEventListener('click', function () {
            month--;
            if (month < 0) {
                month = 11;
                year--;
            }
            changeBackground();
            actData = new Date(year,month,0);
            firstDay = actData.getDay();

            createMonth(month+1, year);
        });

        next.addEventListener('click', function () {
            month++;
            if (month > 11) {
                month = 0;
                year ++;
            }
            changeBackground();
            createMonth(month+1, year);
        });


    }

    createMonth(month+1, year);
    initEvent();

});